import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:training_final_3/data/models/ModelChats.dart';
import 'package:training_final_3/data/models/ModelMessage.dart';
import 'package:training_final_3/data/models/ModelPoints.dart';
import 'package:training_final_3/data/models/ModelProfile.dart';
import 'package:training_final_3/data/models/ModelTransaction.dart';

import '../models/ModelDestinations.dart';
import '../models/ModelPackage.dart';

var supabase = Supabase.instance.client;

Future<AuthResponse> signUp(String email, String password) async {
  return supabase.auth.signUp(email: email, password: password);
}

Future<void> dataSignUp(String userId, String phone, String fullName) async {
  return supabase.from('profiles').insert({
    'id_user': userId,
    'fullname': fullName,
    'phone': phone
  });
}

Future<List<ModelProfile>> getUserData() async {
  var response = await supabase.from('profiles').select();
  return response.map((e) => ModelProfile.fromJson(e)).toList();
}

String? getCurrentUserId() {
  var res = supabase.auth.currentUser?.id;
  return res;
}

Future<AuthResponse> signIn(String email, String password) async {
  return supabase.auth.signInWithPassword(email: email, password: password);
}

Future<void> signOut() async {
  return supabase.auth.signOut();
}

Future<void> sendOTP(String email) async {
  return supabase.auth.resetPasswordForEmail(email);
}

Future<AuthResponse> verifyOTP(String email, String code) {
  return supabase.auth.verifyOTP(
      email: email, token: code, type: OtpType.email);
}

Future<UserResponse> changePassword(String password) {
  return supabase.auth.updateUser(UserAttributes(password: password));
}

Future<void> saveOrders(
    String addressOrder,
    String countryOrder,
    String phoneOrder,
    String othersOrder,
    String items,
    String weight,
    String worth,
    int charges,
    int instant,
    String addressDes,
    String countryDes,
    String phoneDes,
    String othersDes,
    ) async {
  var currentId = getCurrentUserId();
  await supabase.from('orders').insert(
      {
        'id_user': currentId,
        'address': addressOrder,
        'country': countryOrder,
        'phone': phoneOrder,
        'others': othersOrder,
        'package_items': items,
        'weight_items': weight,
        'worth_items': worth,
        'delivery_charges': charges,
        'instant_delivery': instant,
        'tax_and_service_charges': (charges+instant)*0.05,
        'sum_price': charges + instant + (charges+instant)*0.05
      }
  );
  await supabase.from('destinations_details').insert(
      {
        'id_user': currentId,
        'address': addressDes,
        'country': countryDes,
        'phone': phoneDes,
        'others': othersDes,
      }
  );
  await supabase.from('transaction').insert(
    {
      'id_user': currentId,
      'sum': charges + instant + (charges+instant)*0.05,
      'title': 'Delivery fee'
    }
  );
}

Future<List<ModelPoints>> getPoints() async {
  var response = await supabase.from('points').select();
  return response.map((e) => ModelPoints.fromJson(e)).toList();
}

Future<List> getAds() async {
  var response = await supabase.from('ads').select();
  return response.map((e) => e['ads_url']).toList();
}

Future<List<ModelPackage>> getOrderInformation() async {
  var response = await supabase.from('orders').select();
  return response.map((e) => ModelPackage.fromJson(e)).toList();
}

Future<List<ModelDestinations>> getDestinationInformation() async {
  var response = await supabase.from('destinations_details').select();
  return response.map((e) => ModelDestinations.fromJson(e)).toList();
}

Future<List<ModelTransaction>> getTransactions() async {
  var currentId = getCurrentUserId();
  var response = await supabase.from('transactions').select().eq('id_user', currentId!);
  return response.map((e) => ModelTransaction.fromJson(e)).toList();
}

Future<List<ModelChats>> getChats() async {
  var currentId = getCurrentUserId();
  var response = await supabase.from('chats').select().eq('first_user_id', currentId!);
  return response.map((e) => ModelChats.fromJson(e)).toList();
}

Future<ModelProfile> getUserFromId(String id) async {
  var response = await supabase.from('profiles').select().eq('id_user', id);
  return response.map((e) => ModelProfile.fromJson(e)).single;
}

Future<List<ModelMessage>> getUsersMessages(String id) async {
  var response = await supabase.from('messages').select().eq('id_chat', id);
  return response.map((e) => ModelMessage.fromJson(e)).toList();
}
