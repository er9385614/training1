import 'package:flutter/material.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:training_final_3/presentation/pages/sign_up_page.dart';
import 'package:training_final_3/presentation/theme/colors.dart';
import 'package:training_final_3/presentation/theme/theme.dart';

Future<void> main() async {
  await Supabase.initialize(
    url: "https://zlesswvdwrvnumywajpk.supabase.co",
    anonKey: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InpsZXNzd3Zkd3J2bnVteXdhanBrIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTMzNjcwODAsImV4cCI6MjAyODk0MzA4MH0.OQjX2pUbWBk31UvPYAmUfbhyM2T8pvzwTz8V5bucuzI',
  );

  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  MyApp({super.key});

  bool isLight = true;

  void changeColorsApp(BuildContext context){
    isLight = !isLight;
    context.findAncestorStateOfType<_MyAppState>()!.onChangedTheme();
  }

  static MyApp of(BuildContext context){
    return context.findAncestorWidgetOfExactType<MyApp>()!;
  }

  ColorsApp getColors(BuildContext context){
    return (isLight) ? lightColors : darkColors;
  }

  ThemeData getCurrentTheme(){
    return (isLight) ? lightTheme : darkTheme;
  }



  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  void onChangedTheme(){
    setState(() {

    });
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: widget.getCurrentTheme(),
      home: const SignUp(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});


  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold();
  }
}

