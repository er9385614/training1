import 'dart:async';
import 'package:supabase_flutter/supabase_flutter.dart';
import '../data/repository/supabase.dart';
import '../data/storage/seconds.dart';

void time(){
  if (lostSeconds != 0){
    lostSeconds --;
  }
}

void timer(Function callback){
  Timer(
    const Duration(seconds: 1), (){
      time();
      callback();
      timer(callback);
    });
}


Future<void> pressVerifyOTP(String email, String code, Function onResponse, Function(String) onError) async {
  try{
    await verifyOTP(email, code);
    onResponse();
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.toString());
  }on Exception catch(e){
    onError(e.toString());
  }
}