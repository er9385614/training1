import 'package:supabase_flutter/supabase_flutter.dart';

import '../data/repository/supabase.dart';

Future<void> pressSignIn(String email, String password, Function onResponse, Function(String) onError) async {
  try{
    await signIn(email, password);
    onResponse();
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.toString());
  }on Exception catch(e){
    onError(e.toString());
  }
}