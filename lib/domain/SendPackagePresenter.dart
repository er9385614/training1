import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:training_final_3/data/repository/supabase.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart' as yandex;


Future<void> pressSendPackage(String addressOrder,
    String countryOrder,
    String phoneOrder,
    String othersOrder,
    String items,
    String weight,
    String worth,
    int charges,
    int instant,
    String addressDes,
    String countryDes,
    String phoneDes,
    String othersDes,
    Function onResponse,
    Function(String) onError) async {
  try {
    saveOrders(
        addressOrder,
        countryOrder,
        phoneOrder,
        othersOrder,
        items,
        weight,
        worth,
        charges,
        instant,
        addressDes,
        countryDes,
        phoneDes,
        othersDes);
    onResponse();
  } on AuthException catch (e) {
    onError(e.message);
  } on PostgrestException catch (e) {
    onError(e.toString());
  } on Exception catch (e) {
    onError(e.toString());
  }
}

Future<void> geocodePoint(yandex.Point point , Function onResponse, Function(String) onError) async {
  var response = await yandex.YandexSearch.searchByPoint(
      point: point,
      searchOptions: const yandex.SearchOptions(
          searchType: yandex.SearchType.geo,
          geometry: false
      ));
  var res = await response.$2;
}