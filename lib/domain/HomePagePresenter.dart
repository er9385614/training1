import 'dart:typed_data';

import 'package:image_picker/image_picker.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:training_final_3/data/repository/supabase.dart';

Future<void> getNews(Function onResponse, Function(String) onError) async {
  try{
    var res = await getAds();
    onResponse(res);
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.toString());
  }on Exception catch(e){
    onError(e.toString());
  }
}

Future<void> pressPickAvatarFromCamera({required Uint8List avatar, required Function() onPick}) async {
  XFile? avatarFile = await ImagePicker().pickImage(
      source: ImageSource.camera
  );
  if (avatarFile != null){
    avatar = await avatarFile.readAsBytes();
    onPick();
  }
}

Future<void> pressPickAvatarFromGallery({required Uint8List avatar, required Function() onPick}) async {
  XFile? avatarFile = await ImagePicker().pickImage(
      source: ImageSource.gallery
  );
  if (avatarFile != null){
    avatar = await avatarFile.readAsBytes();
    onPick();
  }
}
