import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:training_final_3/data/models/ModelProfile.dart';
import 'package:training_final_3/data/repository/supabase.dart';
import 'package:training_final_3/data/storage/profiles.dart';

Future<void> getProfile(Function onResponse, Function(String) onError) async {
  try{
    profiles = await getUserData();
    var currentId = getCurrentUserId();
    ModelProfile currentUser = profiles.where((element) => element.idUser == currentId).single;
    onResponse(currentUser);
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.toString());
  }on Exception catch(e){
    onError(e.toString());
  }
}

Future<void> pressSignOut(Function onResponse, Function(String) onError) async {
  try{
    await signOut();
    onResponse();
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.toString());
  }on Exception catch(e){
    onError(e.toString());
  }
}