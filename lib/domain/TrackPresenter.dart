import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:training_final_3/data/repository/supabase.dart';

Future<void> getAllPlaceMark(Function onResponse, Function(String) onError) async {
  try{
    var res = await getPoints();
    onResponse(res);
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.toString());
  }on Exception catch(e){
    onError(e.toString());
  }
}