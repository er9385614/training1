import 'package:flutter/material.dart';
import 'package:training_final_3/domain/HomePresenter.dart';

import '../widgets/dialogs.dart';
import 'tabs/home_tab.dart';
import 'tabs/wallet_tab.dart';
import 'tabs/track_tab.dart';
import 'tabs/profile_tab.dart';


class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}
int currentIndex = 0;
class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: BottomNavigationBar(
          elevation: 0,
          type: BottomNavigationBarType.fixed,
          showUnselectedLabels: true,
          currentIndex: currentIndex,
          unselectedLabelStyle: const TextStyle(
              fontSize: 12,
              fontFamily: "Roboto",
              fontStyle: FontStyle.normal,
              fontWeight: FontWeight.w400
          ),
          selectedLabelStyle: const TextStyle(
              fontSize: 12,
              fontFamily: "Roboto",
              fontStyle: FontStyle.normal,
              fontWeight: FontWeight.w400
          ),
          selectedItemColor: const Color.fromARGB(255, 5, 96, 250),
          unselectedItemColor: const Color.fromARGB(255, 167, 167, 167),
          items: [
            BottomNavigationBarItem(
                icon: Image.asset((currentIndex == 0) ? 'assets/house2.png' : 'assets/house1.png'),
                label: "Home"
            ),
            BottomNavigationBarItem(
                icon: Image.asset((currentIndex == 1) ? 'assets/wallet2.png' : 'assets/wallet1.png'),
                label: "Wallet"
            ),
            BottomNavigationBarItem(
                icon: Image.asset((currentIndex == 2) ? 'assets/car2.png' : 'assets/car1.png'),
                label: "Track"
            ),
            BottomNavigationBarItem(
                icon: Image.asset((currentIndex == 3) ? 'assets/profile2.png' : 'assets/profile1.png'),
                label: "Profile"
            ),
          ],
          onTap: (newIndex){
            setState(() {
              currentIndex = newIndex;
            });
          },
        ),
        body: [HomeTab(), const WalletTab(), const TrackTab(), const ProfileTab()][currentIndex]
    );
  }
}