import 'package:flutter/material.dart';
import 'package:training_final_3/main.dart';
import '../widgets/custom_field.dart';
import 'send_package_2.dart';



class SendPackage extends StatefulWidget {
  const SendPackage({super.key});

  @override
  State<SendPackage> createState() => _SendPackageState();

}

class _SendPackageState extends State<SendPackage> {
  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);
    var pointAddress = TextEditingController();
    var pointCountry = TextEditingController();
    var pointPhone = TextEditingController();
    var pointOthers = TextEditingController();
    var destinationAddress = TextEditingController();
    var destinationCountry = TextEditingController();
    var destinationPhone = TextEditingController();
    var destinationOthers = TextEditingController();
    var packageItem = TextEditingController();
    var packageWeight = TextEditingController();
    var packageWorth = TextEditingController();

    return Scaffold(
      backgroundColor: colors.background,
      resizeToAvoidBottomInset: false,
      body: Column(
        children: [
          SizedBox(
            height: 108,
            width: double.infinity,
            child: Column(
              children: [
                SizedBox(
                  height: 106,
                  width: double.infinity,
                  child: Column(
                    children: [
                      const SizedBox(height: 73,),
                      Row(
                        children: [
                          const SizedBox(width: 15),
                          GestureDetector(
                            onTap: (){
                              Navigator.of(context).pop();
                            },
                            child: Image.asset('assets/back.png'),
                          ),
                          const SizedBox(width: 99),
                          Text(
                            'Send a package',
                            style: Theme.of(context).textTheme.titleMedium?.copyWith(fontSize: 16),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 2,
                  decoration: const BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Color.fromARGB(38, 0, 0, 0),
                          blurRadius: 2,
                          offset: Offset(0, 2),
                        )
                      ]
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal:  24),
            child: Column(
              children: [
                SingleChildScrollView(
                  child: Column(
                    children: [
                      const SizedBox(height: 43),
                      Row(
                        children: [
                          Image.asset('assets/point.png'),
                          const SizedBox(width: 8),
                          Text(
                              'Origin Details',
                            style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: 14),
                          )
                        ],
                      ),
                      CustomTextFieldPage(
                          hint: 'Address',
                          controller: pointAddress
                      ),
                      CustomTextFieldPage(
                          hint: 'State,Country',
                          controller: pointCountry
                      ),
                      CustomTextFieldPage(
                          hint: 'Phone number',
                          controller: pointPhone
                      ),
                      CustomTextFieldPage(
                          hint: 'Others',
                          controller: pointOthers
                      ),
                      const SizedBox(height: 39),
                      Row(
                        children: [
                          Image.asset('assets/point2.png'),
                          const SizedBox(width: 8),
                          Text(
                            'Destination Details',
                            style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: 14),
                          )
                        ],
                      ),
                      CustomTextFieldPage(
                          hint: 'Address',
                          controller: destinationAddress
                      ),
                      CustomTextFieldPage(
                          hint: 'State,Country',
                          controller: destinationCountry
                      ),
                      CustomTextFieldPage(
                          hint: 'Phone number',
                          controller: destinationPhone
                      ),
                      CustomTextFieldPage(
                          hint: 'Others',
                          controller: destinationOthers
                      ),
                      const SizedBox(height: 11),
                      Row(
                        children: [
                          Image.asset('assets/add.png'),
                          const SizedBox(width: 6),
                          Text(
                            'Add destination',
                            style: TextStyle(
                              color: colors.subtext,
                              fontSize: 12,
                              fontWeight: FontWeight.w400
                            ),
                          )
                        ],
                      ),
                      const SizedBox(height: 13),
                      Row(
                        children: [
                          Text(
                            'Package Details',
                            style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: 14),
                          ),
                        ],
                      ),
                      CustomTextFieldPage(
                          hint: 'package items',
                          controller: packageItem
                      ),
                      CustomTextFieldPage(
                          hint: 'Weight of item(kg)',
                          controller: packageWeight
                      ),
                      CustomTextFieldPage(
                          hint: 'Worth of Items',
                          controller: packageWorth
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 39),
                Row(
                  children: [
                    Text(
                        'Select delivery type',
                      style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: 14),
                    ),
                  ],
                ),
                const SizedBox(height: 16),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: 159,
                      height: 75,
                      decoration: const BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Color.fromARGB(38, 0, 0, 0),
                              blurRadius: 2,
                              offset: Offset(0, 2),
                            )]
                      ),
                      child: Expanded(
                          child: FilledButton(
                            style: FilledButton.styleFrom(
                              backgroundColor: colors.primary,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)
                              )
                            ),
                              onPressed: (){
                                Navigator.of(context).push(MaterialPageRoute(builder: (context) => SendPackage2(
                                      pointAddress: pointAddress.text,
                                      pointCountry: pointCountry.text,
                                      pointPhone: pointPhone.text,
                                      pointOthers: pointOthers.text,
                                      destinationAddress: destinationAddress.text,
                                      destinationCountry: destinationCountry.text,
                                      destinationPhone: destinationPhone.text,
                                      destinationOthers: destinationOthers.text,
                                      packageItem: packageItem.text,
                                      packageWeight: packageWeight.text,
                                      packageWorth: packageWorth.text,
                                    )
                                  )
                                );
                              },
                              child: Column(
                                children: [
                                  const SizedBox(height: 13,),
                                  Image.asset('assets/clock.png'),
                                  const SizedBox(height: 10),
                                  const Text(
                                    'Instant delivery',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 14
                                    ),
                                  )
                                ],
                              )
                          )
                      ),
                    ),
                    const SizedBox(width: 24),
                    Container(
                      width: 159,
                      height: 75,
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                          boxShadow: [
                            BoxShadow(
                              color: Color.fromARGB(38, 0, 0, 0),
                              blurRadius: 2,
                              offset: Offset(0, 2),
                            )]
                      ),
                      child: OutlinedButton(
                        style: OutlinedButton.styleFrom(
                          side: const BorderSide(color: Colors.white),
                            backgroundColor: colors.background,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)
                            )
                        ),
                          onPressed: (){},
                          child: Column(
                            children: [
                              const SizedBox(height: 13,),
                              Image.asset('assets/time.png'),
                              const SizedBox(height: 10),
                              Text(
                                'Scheduled delivery',
                                style: TextStyle(
                                    color: colors.subtext,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 12
                                ),
                              )
                            ],
                          )
                      ),
                    )
                  ],
                )
              ],
            )
          ),
        ],
      )
    );
  }
}