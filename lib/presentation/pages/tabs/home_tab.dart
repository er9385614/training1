import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:training_final_3/presentation/pages/send_package.dart';

class HomeTab extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          FilledButton(onPressed: (){
            Navigator.of(context).push(
                MaterialPageRoute(
                    builder: (_) => SendPackage()
                )
            );
          }, child: Text(""))
        ],
      ),
    );
  }
}