import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:training_final_3/domain/WalletPresenter.dart';
import 'package:training_final_3/presentation/pages/add_payment.dart';
import 'package:training_final_3/presentation/widgets/dialogs.dart';
import '../../../data/models/ModelProfile.dart';
import '../../../data/models/ModelTransaction.dart';
import '../../../domain/HomePresenter.dart';
import '../../theme/colors.dart';


class WalletTab extends StatefulWidget {
  const WalletTab({super.key});

  @override
  State<WalletTab> createState() => _WalletTabState();
}
var name = '';
var balance = '';
bool isSee = true;
List<ModelTransaction> transactions = [];
class _WalletTabState extends State<WalletTab> {
  @override
  void initState(){
    super.initState();
    getProfile(
            (ModelProfile user){
          name = user.fullName;
          balance = user.balance.toString();
        },
        (String e){showError(context, e);}
    );
    getUserTransactions(
        (res){
          transactions = res;
        },
        (String e){showError(context, e);}
    );
  }
  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Scaffold(
      backgroundColor: colors.background,
      body: Column(
        children: [
          SizedBox(
            height: 108,
            width: double.infinity,
            child: Column(
              children: [
                const SizedBox(height: 73),
                Center(
                  child: Text(
                    'Wallet',
                    style: Theme.of(context).textTheme.titleMedium?.copyWith(fontSize: 16),
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: 2,
            decoration: const BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Color.fromARGB(38, 0, 0, 0),
                    blurRadius: 2,
                    offset: Offset(0, 2),
                  )]
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 17, left: 24, right: 24),
            child: Column(
              children: [
                SizedBox(
                  height: 75,
                  child: ListTile(
                    contentPadding: const EdgeInsets.symmetric(vertical: 10),
                    leading: Container(
                        width: 60,
                        height: 60,
                        decoration: const BoxDecoration(
                            shape: BoxShape.circle,
                            color: Color.fromARGB(255, 207, 207, 207)
                        ),
                        child: const Icon(Icons.person, color: Color.fromARGB(255, 58, 58, 58))
                    ),
                    title: Text(
                      'Hello $name',
                      style: const TextStyle(
                          color: Color.fromARGB(255, 58, 58, 58),
                          fontSize: 16,
                          fontFamily: "Roboto",
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500
                      ),
                    ),
                    subtitle: RichText(
                      text: TextSpan(
                          children: [
                            const TextSpan(
                                text: "Current balance: ",
                                style: TextStyle(
                                    color: Color.fromARGB(255, 58, 58, 58),
                                    fontSize: 12,
                                    fontFamily: "Roboto",
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.w400
                                )
                            ),
                            TextSpan(
                                text: (isSee) ? "N$balance:00" : "*"*8,
                                style: const TextStyle(
                                    color: Color.fromARGB(255, 5, 96, 250),
                                    fontSize: 12,
                                    fontFamily: "Roboto",
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.w500
                                )
                            )
                          ]
                      ),
                    ),
                    trailing: GestureDetector(
                        onTap: (){
                          setState(() {
                            isSee = !isSee;
                          });
                        },
                        child: SvgPicture.asset('assets/eye-slash.svg')
                    ),
                  ),
                ),
                const SizedBox(height: 28),
                Container(
                  width: double.infinity,
                  height: 116,
                  decoration: BoxDecoration(
                      color: colors.hint,
                      borderRadius: BorderRadius.circular(8)
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const SizedBox(height: 10),
                      Text(
                          'Top Up',
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w700,
                              color: colors.text
                          )
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 10, left: 48, right: 48),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              children: [
                                Container(
                                  height: 48,
                                  width: 48,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(360),
                                      color: colors.primary
                                  ),
                                  child: Image.asset('assets/museum.png'),
                                ),
                                const SizedBox(height: 4,),
                                Text(
                                  'Bank',
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: colors.text
                                  ),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                Container(
                                  height: 48,
                                  width: 48,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(360),
                                      color: colors.primary
                                  ),
                                  child: Image.asset('assets/toback.png'),
                                ),
                                const SizedBox(height: 4,),
                                Text(
                                  'Transfer',
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: colors.text
                                  ),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                GestureDetector(
                                  onTap: (){
                                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => const AddPaymentPage()));
                                  },
                                  child: Container(
                                    height: 48,
                                    width: 48,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(360),
                                        color: colors.primary
                                    ),
                                    child: Image.asset('assets/card.png'),
                                  ),
                                ),
                                const SizedBox(height: 4,),
                                Text(
                                  'Card',
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: colors.text
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                const SizedBox(height: 41),
                Row(
                  children: [
                    Text(
                      'Transaction History',
                      style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: 20),
                    ),
                  ],
                ),
                const SizedBox(height: 24),
                SizedBox(
                  height: 320,
                  width: double.infinity,
                  child: ListView.builder(
                    itemCount: transactions.length,
                      itemBuilder: (_, index){
                      return Container(
                        height: 44,
                        width: double.infinity,
                        decoration: BoxDecoration(
                            color: colors.background,
                            boxShadow: const [
                              BoxShadow(
                                color: Color.fromARGB(38, 0, 0, 0),
                                blurRadius: 2,
                                offset: Offset(0, 2),
                              )]
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 12),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                children: [
                                  Text(
                                    transactions[index].sum.toString(),
                                    style: TextStyle(
                                        color: colors.error,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 16
                                    ),
                                  ),
                                  Text(
                                      transactions[index].title,
                                      style: TextStyle(
                                          color: colors.text,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 12
                                      )
                                  )
                                ],
                              ),
                              Text(
                                transactions[index].formatDate(),
                                style: TextStyle(
                                    color: colors.subtext,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400
                                ),
                              )
                            ],
                          ),
                        ),
                      );
                      }
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}