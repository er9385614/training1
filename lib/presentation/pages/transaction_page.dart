import 'package:flutter/material.dart';
import '../../main.dart';
import 'home_page.dart';

class Transaction extends StatefulWidget {
  const Transaction({super.key});

  @override
  State<Transaction> createState() => _TransactionState();
}

class _TransactionState extends State<Transaction> {
  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);
    return Scaffold(
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 144, left: 137, right: 134),
            child: SizedBox(
              height: 119,
              width: 119,
              child: Expanded(
                child: CircularProgressIndicator(
                  backgroundColor: Colors.transparent,
                  valueColor: AlwaysStoppedAnimation<Color>(colors.secondary),
                  strokeWidth: 13.88,
                  strokeCap: StrokeCap.round,
                ),
              ),
            ),
          ),
          const SizedBox(height: 130,),
          Text(
            'Your rider is on the way to your destination',
            style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 14,
              color: colors.text
            ),
          ),
          const SizedBox(height: 8,),
          RichText(text: TextSpan(
            children: [
              TextSpan(
                text: 'Tracking Number ',
                style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 14,
                    color: colors.text
                ),
              ),
              TextSpan(
                text: 'R-7458-4567-4434-5854',
                style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 14,
                    color: colors.primary
                ),
              )
            ]
          )),
          Padding(
              padding: const EdgeInsets.only(top: 141, left: 24, right: 24),
            child: Column(
              children: [
                SizedBox(
                  height: 46,
                  width: double.infinity,
                  child: FilledButton(
                      onPressed: (){
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => const Home()));
                      },
                      style: Theme.of(context).filledButtonTheme.style,
                      child: Text(
                        'Track my item',
                        style: Theme.of(context).textTheme.labelMedium
                      )
                  ),
                ),
                const SizedBox(height: 8,),
                SizedBox(
                  height: 46,
                  width: double.infinity,
                  child: OutlinedButton(
                      onPressed: (){
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => const Home()));
                      },
                      style: OutlinedButton.styleFrom(
                          side: BorderSide(color: colors.primary),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(4)
                          )
                      ),
                      child: Text(
                        'Go back to homepage',
                        style: TextStyle(
                            color: colors.primary,
                            fontSize: 16,
                            fontWeight: FontWeight.w700
                        ),
                      )
                  ),
                ),
              ],
            ),
          )

        ],
      ),
    );
  }
}
