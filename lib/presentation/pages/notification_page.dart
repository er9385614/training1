import 'package:flutter/material.dart';

class NotificationPage extends StatefulWidget {
  const NotificationPage({super.key});

  @override
  State<NotificationPage> createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SizedBox(
            height: 108,
            width: double.infinity,
            child: Column(
              children: [
                SizedBox(height: 73,),
                Row(
                  children: [
                    SizedBox(width: 15),
                    GestureDetector(
                      onTap: (){
                        Navigator.of(context).pop();
                      },
                      child: Image.asset('assets/back.png'),
                    ),
                    const SizedBox(width: 114),
                    Text(
                      'Notification',
                      style: Theme.of(context).textTheme.titleMedium?.copyWith(fontSize: 16),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container(
            height: 2,
            decoration: const BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Color.fromARGB(38, 0, 0, 0),
                    blurRadius: 2,
                    offset: Offset(0, 2),
                  )
                ]
              ),
            ),
          SizedBox(height: 120),
          Center(
            child: Column(
              children: [
                Image.asset('assets/notification_page.png'),
                SizedBox(height: 18),
                Text(
                    'You have no notifications',
                  style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: 16),
                )
              ],
            ),
          )
          ]
        ),
    );
  }
}